function togglePasswordVisibility(inputId) {
    const input = document.getElementById(inputId);
    const icon = input.nextElementSibling;
    if (input.type === 'password') {
      input.type = 'text';
      icon.classList.remove('fa-eye');
      icon.classList.add('fa-eye-slash');
    } else {
      input.type = 'password';
      icon.classList.remove('fa-eye-slash');
      icon.classList.add('fa-eye');
    }
  }

  function validatePassword(event) {
    event.preventDefault();
    const password = document.getElementById('passwordInput').value;
    const confirmPassword = document.getElementById('confirmPasswordInput').value;

    if (password === confirmPassword) {
      alert('You are welcome');
    } else {
      const errorMessage = document.createElement('p');
      errorMessage.textContent = 'Потрібно ввести однакові значення';
      errorMessage.style.color = 'red';
      const form = document.querySelector('.password-form');
      form.appendChild(errorMessage);
    }
  }